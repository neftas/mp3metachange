# MP3MetaChange
A very light-weight program to change the meta data of mp3's quickly

MP3MetaChanges allows you to select (or drag&drop) mp3's and quickly and conveniently edit their meta data individually, or in a batch. 

MP3MetaChange depends on `id3v2` and `yad` and requires Bash 4.3+.

## How to install

Installing is easy. Open a terminal and enter (copy & paste):

    su -c "wget 'https://raw.githubusercontent.com/Neftas/mp3metachange/master/mp3metachange-installer' \
    -O mp3metachange-installer && chmod +x mp3metachange-installer && ./mp3metachange-installer"
    
Enter your password and you're all set.
