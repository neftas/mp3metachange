#!/bin/bash
# Copyright (c) Stefan van den Akker, 2015 <https://github.com/Neftas/mp3metachange>
#
# This file is part of MP3MetaChange.
#
# MP3MetaChange is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MP3MetaChange is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MP3MetaChange.  If not, see <http://www.gnu.org/licenses/>.
#
# Description: select MP3 files and change their metadata in batch
# or individually from a list

# globals
##################################################

PROG_NAME="MP3MetaChange"
ICON="/usr/share/icons/hicolor/scalable/apps/mp3metachange.png"
OLD_IFS="$IFS"

# dimensions of dialogs
##################################################

WIDTH_BIG=800
WIDTH_SMALL=400
HEIGHT_BIG=600
HEIGHT_SMALL=300

# define colors
##################################################

BOLD="\033[1;10m"
RED="\033[0;31m"
REDBOLD="\033[1;31m"
GREEN="\033[0;32m"
GREENBOLD="\033[1;32m"
YELLOW="\033[0;33m"
YELLOWBOLD="\033[1;33m"
BLUE="\033[0;34m"
BLUEBOLD="\033[1;34m"
ENDCOLOR="\033[0m"

# global arrays
##################################################

FILENAMES=''                # contains filenames selected by user
BATCH_CHANGES=''            # structure:
                            # PATH|FILENAME|ARTIST|TITLE|ALBUM|TRNUM|YEAR|GENRE
declare -a DATABASE         # structure identitcal to BATCH_CHANGES
declare -a CHANGED_DATABASE # idem
declare -a DELETE_LIST      # idem

# array of genres
##################################################
GENRES=(
"Blues" "Classic Rock" "Country" "Dance" "Disco" "Funk" "Grunge" "Hip-Hop" "Jazz"
"Metal" "New Age" "Oldies" "Other" "Pop" "R&B" "Rap" "Reggae" "Rock" "Techno"
"Industrial" "Alternative" "Ska" "Death Metal" "Pranks" "Soundtrack"
"Euro-Techno" "Ambient" "Trip-Hop" "Vocal" "Jazz+Funk" "Fusion" "Trance"
"Classical" "Instrumental" "Acid" "House" "Game" "Sound Clip" "Gospel"
"Noise" "AlternRock" "Bass" "Soul" "Punk" "Space" "Meditative"
"Instrumental Pop" "Instrumental Rock" "Ethnic" "Gothic" "Darkwave"
"Techno-Industrial" "Electronic" "Pop-Folk" "Eurodance" "Dream" "Southern Rock"
"Comedy" "Cult" "Gangsta" "Top 40" "Christian Rap" "Pop/Funk" "Jungle"
"Native American" "Cabaret" "New Wave" "Psychadelic" "Rave" "Showtunes"
"Trailer" "Lo-Fi" "Tribal" "Acid Punk" "Acid Jazz" "Polka" "Retro" "Musical"
"Rock & Roll" "Hard Rock"
)

declare -A GENRES_AA
j=0
for elem in "${GENRES[@]}"; do
    GENRES_AA["$elem"]=$j
    let j+=1
done
unset j

# flags
##################################################

ADD_FLAG="false"            # flag set when user adds files in list view

# helper functions
##################################################

##################################################
# Check if element is present in an array.
# Globals:
#   None
# Arguments:
#   $1 element
#   $2 array
# Returns:
#   Return 0 if element is present in an array, 1 otherwise.
##################################################
contains_element() {
    local elem
    for elem in "${@:2}"; do
        if [[ "$elem" == "$1" ]]; then
            return 0
        fi
    done
    return 1
}

##################################################
# Check if path is present in global array DATABASE.
# Globals:
#   DATABASE
# Arguments:
#   $1 path
# Returns:
#   0 if path is present in DATABASE, 1 otherwise
##################################################
path_in_db() {
    declare -a filenames
    local elem

    for elem in "${DATABASE[@]}"; do
        filenames+=("${elem%%|*}")
    done

    contains_element "$1" "${filenames[@]}"

    return $?
}

##################################################
# Join arguments together in one string with a delimiter.
# Globals:
#   None
# Arguments:
#   $1 delimiter
#   $2 array or string
#   $3...$n arbitrary number of arguments
# Returns:
#   None
##################################################
join() {
    local IFS="$1"
    shift
    echo "$*"
}

##################################################
# Capitalize each word of the first argument
# Globals:
#   None
# Arguments:
#   $1 string
# Returns:
#   None
##################################################
capitalize() {
    declare -a words
    read -r -a words <<< "${1,,}"
    echo "${words[*]^}"
}

##################################################
# Check if argument is a positive integer.
# Globals:
#   None
# Arguments:
#   $1 string
# Returns:
#   Return 0 when argument is a positive integer,
#   2 when it is the special value -1, and 1 otherwise.
##################################################
is_positive_integer() {
    case "$1" in
        ''|*[!0-9]*)
            return 1;;
        -1)
            return 2;;
        *)
            return 0;;
    esac
}

# main program
##################################################

##################################################
# Show a dialog in which the user has to confirm a pending action.
# Globals:
#   ICON
# Arguments:
#   None
# Returns:
#   Return 0 for affirmative/yes, 1 for negative/cancel.
##################################################
confirmation_dialog() {
    yad --width=200 --height=100 --image=dialog-warning \
        --window-icon="$ICON" --title="Confirmation" \
        --text="Are you sure?" --text-align=center \
        --image-on-top --fixed --center \
        --button="Exit:3" --button="Back:1" --button="Yes:0"
    local ret=$?
    if [[ $ret == 3 ]]; then
        echo "Exiting from confirmation_dialog..." >&2
        exit 1;
    elif [[ $ret == 252 ]]; then
        echo "confirmation_dialog closed" >&2
        # return to the function that called the confirmation dialog
        $(caller 0 | cut -d " " -f 2)
    else
        return $ret
    fi
}

##################################################
# Apply changes that were made in change_batch().
# Globals:
#   BATCH_CHANGES
#   GENRES_AA
#   DATABASE
#   CHANGED_DATABASE
# Arguments:
#   None
# Returns:
#   None
##################################################
apply_batch_changes() {
    echo "BATCH_CHANGES at beginning of apply_batch_changes: $BATCH_CHANGES" >&2
    sanitize_db_input BATCH_CHANGES

    local num_files_changed=0
    IFS='|' read -r -a batch_array <<< "${BATCH_CHANGES}"
    echo "batch_array after sanitizing BATCH_CHANGES: ${batch_array[*]}" >&2

    # BATCH_CHANGES looks like: ARTIST|HINT|TITLE|ALBUM|TRACK|YEAR|GENRE
    local artist="${batch_array[2]}"
    local title="${batch_array[3]}"
    local title_org="$title" # store original filename
    local album="${batch_array[4]}"
    local start_num="${batch_array[5]%%,*}"
    local year="${batch_array[6]}"
    local genre="${batch_array[7]}"

    echo "batch_array assigned to individual variables: \
        $artist|$title|$album|$start_num|$year|$genre" >&2

    # change genre to a number
    genre=${GENRES_AA[$genre]}

    # if title contains the metasequence !n at some point
    # we will input the track number there
    local regex='!n'
    local alt_num_loc="false"
    if [[ "$title" =~ $regex ]]; then
        echo "alt_num_loc is set to TRUE" >&2
        alt_num_loc="true"
    fi

    # we want track numbers to look like 01, 02, 10, 99, 121 etc.
    # so we take the number of tracks in DATABASE and make sure
    # all numbers take leading zeroes to have the same length everywhere
    local len_array=${#DATABASE[@]}
    local num_zeroes=2
    if [[ $len_array -gt 99 ]]; then
        num_zeroes=${#len_array}
    fi
    local i=$start_num

    # we respect the order the user has selected in the list
    # so we'll use the user-defined CHANGED_DATABASE rather
    # than DATABASE
    for elem in "${CHANGED_DATABASE[@]}"; do
        local file="${elem%%|*}"
        if [[ $alt_num_loc = "true" ]]; then
            local num=
            num=$(printf "%0${num_zeroes}g" $i)
            title="${title_org/\!n/$num}"
        fi
        echo "Changing meta data of '$file' to \
            $artist:$album:$title:$year:$genre" >&2
        id3v2 -a "$artist" -A "$album" -t "$title" \
            -T "$i"/"$len_array" -y "$year" -g "$genre" "$file"
        let i+=1
        let num_files_changed+=1
    done

    show_results $num_files_changed
}

##################################################
# Change the fields of all files in the database in a batch.
# Globals:
#   GENRES
#   DATABASE
#   CHANGED_DATABASE
#   BATCH_CHANGES
# Arguments:
#   None
# Returns:
#   None
##################################################
change_batch() {
    # in order to differentiate tracks in a batch of files
    # we need a way to make them different; so we append
    # the track number to each song title or the user can
    # select where to put it by putting the metasequence !n in the title

    # prepare default values
    declare -a def_values
    IFS='|' read -r -a def_values <<< "${DATABASE[0]}"
    local def_artist="${def_values[2]}"
    local def_title="${def_values[3]}"
    local def_album="${def_values[4]}"
    local def_num=1
    local def_year="${def_values[6]}"
    local def_genre="${def_values[7]}"
    local hint="Hint: put !n where you want to insert the track number"

    echo "Values: $def_artist|$def_title|$def_album|\
        $def_num|$def_year|$def_genre" >&2

    # check if we can find a number in the title to
    # put in def_num as default value
    local regex='([0-9]+)'
    if [[ "$def_title" =~ $regex ]]; then
        def_num="${BASH_REMATCH[1]}"
        # strip all leading zeroes from def_num
        def_num="${def_num##*0}"
    fi

    # select columns the user wants to change
    BATCH_CHANGES=$(yad --center --width=$((WIDTH_BIG / 2)) \
        --window-icon="$ICON" \
        --title="Batch change" \
        --align="right" \
        --form \
        --field="Artist" --field="$hint:LBL" \
        --field="Title" --field="Album" \
        --field="Starting #:NUM" \
        --field="Year:DT" --field="Genre:CB" \
        --date-format="%Y" \
        "$def_artist" "$hint" "$def_title" "$def_album" \
        "$def_num!1..10000!1" "$def_year" "$(join ! "${GENRES[@]}")" \
        --button="Back:3" --button="Exit:1" --button="Apply:0")

    # edit BATCH_CHANGES to conform to format of DATABASE so we can compare them
    local IFS="|"
    read -r -d '' -a temp_arr <<< "$BATCH_CHANGES"
    unset temp_arr[1]
    temp_arr[4]="${temp_arr[4]%%,*}"
    BATCH_CHANGES="||""${temp_arr[*]}"

    local ret=$?
    case $ret in
        0)
            echo "Applying changes in change_batch...";;
        1)
            echo "Cancelling from change_batch..." >&2
            exit 1;;
        3|252)
            file_list
            return;;
    esac

    confirmation_dialog

    ret=$?
    case $ret in
        0)
            apply_batch_changes
            return;;
        1)
            change_batch
            return;;
    esac
}

##################################################
# Open a file manager to look for mp3 files.
# Globals:
#   FILENAMES
#   WIDTH_BIG
#   HEIGHT_BIG
#   ICON
# Arguments:
#   None
# Returns:
#   0 to fill the database with the selected files
#   1 to exit
#   2 or 252 to return to the begin screen
##################################################
file_manager() {
    FILENAMES=$(yad --center --width=$WIDTH_BIG --height=$HEIGHT_BIG \
            --window-icon="$ICON" \
            --file --multiple \
            --file-filter="mp3 files | *.mp3 *.MP3" \
            --button="Exit:1" \
            --button="Back:2" --button="Select:0")
    local ret=$?
    case $ret in
        0)
            fill_database
            return;;
        1)
            echo "Cancelled from file_manager" >&2
            exit 1;;
        2)
            begin
            return;;
        252)
            begin
            return;;
    esac
}

##################################################
# Show a dialog with the number of files processed.
# Globals:
#   WIDTH_SMALL
#   HEIGHT_SMALL
#   ICON
# Arguments:
#   None
# Returns:
#   0 or 252 to exit the program
#   1 to return to the start screen
##################################################
show_results() {
    yad --title="$1 files succesfully changed" --window-icon="$ICON" \
        --width=$WIDTH_SMALL --height=$((HEIGHT_SMALL / 2)) \
        --center \
        --image="$ICON" --image-on-top \
        --text="$1 files were succesfully changed" \
        --text-align="center" \
        --button="Start over:1" --button="Quit:0"
    local ret=$?
    case $ret in
        0|252)
            exit 0;;
        1)
            begin
            return;;
    esac
}

##################################################
# Sanitize the input received from the user, so that
# files cannot be corrupted by faulty input.
# Globals:
#   GENRES_AA
# Arguments:
#   $1 array
# Returns:
#   None
##################################################
sanitize_db_input() {
    local -n arr=$1
    local elem
    local i
    local j
    local index=0
    local genre
    declare -a item

    for elem in "${arr[@]}"; do
        IFS="|" read -r -a item <<< "$elem"
        # empty strings are converted to "default"
        for i in {2..4}; do
            if [[ "${item[$i]}" == "" ]]; then
                echo "Item is empty: item[$i]" >&2
                item[$i]="default"
            fi
        done
        # strings and negative numbers are replaced with the empty string
        for j in {5..6}; do
            is_positive_integer "${item[$j]}"
            if [[ $? == 1 || $? == 2 ]]; then
                echo "Not a positive integer: item[$j]" >&2
                item[$j]=''
            fi
        done

        # genre is either a positive integer or a keyword
        genre=$(capitalize "${item[7]}")
        is_positive_integer "$genre"
        local ret=$?
        # if genre is a number that is greater than 255
        # or is a string that is not present in GENRES_AA
        # we set the corresponding entry to 255 for "Unknown"
        if [[ ( $ret == 0 && "$genre" > 255) || ( $ret == 1 && ! "${GENRES_AA[$genre]}" ) ]]; then
            echo "Setting genre to default of 255..." >&2
            item[7]=255
        fi

        local IFS="|"
        result="${item[*]}"
        echo "result looks like: $result"
        arr[index]="$result"
        let index+=1
    done

    echo "arr after sanitizing: ${arr[*]}"
}

##################################################
# Check for changes made in the list view and apply them.
# Globals:
#   DATABASE
#   CHANGED_DATABASE
# Arguments:
#   None
# Returns:
#   None
##################################################
check_changes_and_save() {
    # echo "DEBUG: check_changes_and_save" >&2
    # echo "DATABASE: ${DATABASE[@]}" >&2
    # echo "CHANGED_DATABASE ${CHANGED_DATABASE[@]}" >&2

    # first, sanitize the user input
    sanitize_db_input CHANGED_DATABASE

    local num_files_changed=0

    for elem in "${DATABASE[@]}"; do
        for mele in "${CHANGED_DATABASE[@]}"; do
            # put both elem and mele in an array, so we can
            # more easily access the different elements
            declare -a elem_array
            declare -a mele_array
            IFS="|" read -r -a elem_array <<< "$elem"
            IFS="|" read -r -a mele_array <<< "$mele"
            declare -p elem_array >&2
            declare -p mele_array >&2
            # if the data has changed, save the new data to file
            if [[ "${elem_array[0]}" == "${mele_array[0]}" ]]; then
                if [[ "${elem_array[@]}" != "${mele_array[@]}" ]]; then
                    echo -e $(cat <<- EOF
			\tArtist:\t\tTitle:\t\t\tAlbum:\t\tTrack:\tYear:\tGenre:\n
			Old:\t${elem_array[2]}\t${elem_array[3]}\t\t${elem_array[4]}\t
			${elem_array[5]}\t${elem_array[6]}\t${elem_array[7]}\n
			New:\t${mele_array[2]}\t${mele_array[3]}\t\t${mele_array[4]}\t
			${mele_array[5]}\t${mele_array[6]}\t${mele_array[7]}
			EOF
                    )
                    id3v2 -a "${mele_array[2]}" -t "${mele_array[3]}" \
                        -A "${mele_array[4]}" -T "${mele_array[5]}" \
                        -y "${mele_array[6]}" -g "${mele_array[7]}" \
                        "${mele_array[0]}"
                    let num_files_changed+=1
                else
                    break 2
                fi
            fi
        done
    done

    show_results $num_files_changed
}

##################################################
# Fill the DATABASE with the data from the selected files.
# Globals:
#   FILENAMES
#   ADD_FLAG
#   DATABASE
#   IFS
#   OLD_IFS
# Arguments:
#   None
# Returns:
#   None
##################################################
fill_database() {
    if [[ ! $FILENAMES ]]; then
        echo "FILENAMES is empty!" >&2
        begin
        return
    fi
    # if ADD_FLAG is not set, empty database first
    if [[ $ADD_FLAG = "false" ]]; then
        DATABASE=()
    else
        ADD_FLAG="false"
    fi
    # create local array
    declare -a choice_array
    echo "FILENAMES: $FILENAMES" >&2
    # put in a nice array
    declare -a choice_array
    local check_pipe_regex='.*\|.*'
    if [[ $FILENAMES =~ $check_pipe_regex ]]; then
        echo "FILENAMES seperated by pipe characters" >&2
        IFS="|"
        for line in $FILENAMES; do
            # filter duplicates from the add functionaly
            path_in_db "$line"
            if [[ $? == 0 ]]; then
                continue
            else
                choice_array+=( "$line" )
            fi
        done
        IFS="$OLD_IFS"
    else
        echo "FILENAMES is not seperated by pipe characters" >&2
        IFS= read -r -d '' choice_array <<< "$FILENAMES"
    fi

    echo "choice_array: ${choice_array[@]}" >&2
    # sort array on title and make sure duplicates are filtered
    IFS=$'\n' choice_array=($(sort -t \| -k4 <<< "${choice_array[*]}" | uniq))
    IFS="$OLD_IFS"
    # put all filenames in a list
    for f in "${choice_array[@]}"; do
        local file="${f#*file://}"
        echo "${file}" >&2
        echo "${file##*/}" >&2
        local result=$(id3v2 -l "${file}")
        local regex_artist='^TPE1.*: \K(.*)$'
        local artist=$(echo "$result" | grep -P -o "$regex_artist")
        echo "$artist" >&2
        local regex_title='TIT2.*: \K(.*)$'
        local title=$(echo "$result" | grep -P -o "$regex_title")
        echo "$title" >&2
        local regex_album='TALB.*: \K(.*)$'
        local album=$(echo "$result" | grep -P -o "$regex_album")
        echo "$album" >&2
        local regex_num='TRCK.*: \K(.*)$'
        local num=$(echo "$result" | grep -P -o "$regex_num")
        echo "$num" >&2
        local regex_year='Year: \K([0-9]+)'
        local year=$(echo "$result" | grep -P -o "$regex_year")
        echo "$year" >&2
        local regex_genre='TCON.*:.*\(\K([0-9]+)'
        local genre=$(echo "$result" | grep -P -o "$regex_genre")
        echo "$genre" >&2
        DATABASE+=("$file|${file##*/}|$artist|$title|$album|$num|$year|$genre|")
    done

    file_list
}

##################################################
# Remove items selected in the list view from the DATABASE.
# Globals:
#   DATABASE
#   DELETE_LIST
# Arguments:
#   None
# Returns:
#   None
##################################################
remove_item_from_database() {
    local len_db=${#DATABASE[@]}
    local len_delete=${#DELETE_LIST[@]}

    for ((i=0; i < len_db; i++)); do
        for ((j=0; j < len_delete; j++)); do
            echo "DATABASE[$i]=${DATABASE[$i]}" >&2
            echo "DELETE_LIST[$j]=${DELETE_LIST[$j]}" >&2
            if [[ "${DATABASE[$i]%%|*}" == "${DELETE_LIST[$j]%%|*}" ]]; then
                echo "Going to unset DATABASE[$i], i.e.: ${DATABASE[$i]}" >&2
                unset DATABASE[$i]
            fi
        done
    done

    file_list
}

##################################################
# List the selected files in a window.
# Globals:
#   IFS
#   DATABASE
#   DELETE_LIST
#   CHANGED_DATABASE
#   WIDTH_BIG
#   HEIGHT_BIG
#   ICON
# Arguments:
#   None
# Returns:
#   None
##################################################
file_list() {
    # echo "IFS at the beginning of function file_list:" >&2
    # printf "%s" "$IFS" | od -bc >&2
    local i=0
    local change_files=
    change_files=$(
        IFS='|'
        for line in "${DATABASE[@]}"; do
            echo "FALSE"
            for elem in ${line}; do
                # check if element is empty and if the empty
                # empty element is the 5th 6th or 7th element
                if [[ ! "$elem" && "567" == *${i}* ]]; then
                    echo "-1"
                else
                    echo "$elem"
                fi
                let i+=1
            done
            i=0
        done |
        yad --center --width=$WIDTH_BIG --height=$HEIGHT_BIG \
        --window-icon="$ICON" --title="Select files you want to change" \
        --list --multiple \
        --print-all --editable --search-column="5" \
        --column="Delete?:CHK" \
        --column="Path:HD" --column="Filename" --column="Artist" \
        --column="Title" --column="Album" --column="Track:NUM" \
        --column="Year:NUM" --column="Genre:NUM" \
        --button="Exit:1" --button="Start over:3" \
        --button="Remove selected:6" \
        --button="Add more...:5" --button="Change batch...:2" \
        --button="Apply changes:0")
    local ret=$?

    # echo "IFS after the subshell returned:" >&2
    # printf "%s" "$IFS" | od -bc >&2
    # create a new list that we can compare with the old
    declare -p change_files
    # put only files starting with TRUE| in DELETE_LIST
    IFS=$'\n' read -r -d '' -a DELETE_LIST < <(
        IFS=$'\n'
        for elem in $change_files; do
            if [[ "${elem%%|*}" = TRUE ]]; then
                echo "${elem#*|}"
            fi
        done)
    # remove leading TRUE| or FALSE| value from CHANGED_DATABASE
    IFS=$'\n' read -r -d '' -a CHANGED_DATABASE < <(
        IFS=$'\n'
        for elem in $change_files; do
            echo "${elem#*|}"
        done)
    # echo "CHANGED_DATABASE[@]: ${CHANGED_DATABASE[@]}"
    # echo "CHANGED_DATABASE[0]: ${CHANGED_DATABASE[0]}"
    case $ret in
        0)
            echo "Applying in file_list..." >&2 ;;
        1|252)
            echo "Exiting from file_list" >&2
            exit 1;;
        2)
            change_batch
            return;;
        3)
            begin
            return;;
        5)
            ADD_FLAG="true"
            file_manager
            return;;
        6)
            remove_item_from_database
            return;;
    esac

    confirmation_dialog

    ret=$?
    case $ret in
        0)
            check_changes_and_save
            return;;
        1)
            file_list
            return;;
    esac
}

##################################################
# Begin point of program.
# Globals:
#   FILENAMES
#   WIDTH_SMALL
#   HEIGHT_SMALL
#   ICON
#   PROG_NAME
# Arguments:
#   None
# Returns:
#   None
##################################################
begin() {
    # make sure string FILENAMES is empty at each run of begin
    FILENAMES=
    local text="You can drag&amp;drop files here or press the\n\
    	<b>Select files...</b> button to open your file manager"
    FILENAMES=$(yad --center --width=$WIDTH_SMALL --height=$HEIGHT_SMALL \
        --window-icon="$ICON" \
        --image="$ICON" --image-on-top \
        --fixed --buttons-layout=spread \
        --title="$PROG_NAME" --text-align="center" \
        --text="$(echo <<<- $text)" \
        --dnd \
        --button="Exit:1" \
        --button="Select files...:2" --button="Process files...:0")
    local ret=$?
    case $ret in
        0)
            fill_database
            return;;
        1)
            echo "Nothing selected. Exiting..." >&2
            exit 1;;
        2)
            file_manager
            return;;
    esac
}

begin
