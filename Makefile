INSTALL=install
INSTALL_DIRECTORY=$(INSTALL) -dm 755
INSTALL_PROGRAM=$(INSTALL) -Dpm 0755
INSTALL_DATA=$(INSTALL) -Dpm 0644

install:
	$(INSTALL_DIRECTORY) /usr/share/mp3metachange
	$(INSTALL_PROGRAM) mp3metachange.sh -t /usr/share/mp3metachange/
	$(INSTALL_DATA) mp3metachange.desktop -t /usr/share/applications/
	$(INSTALL_DATA) mp3metachange.png -t /usr/share/icons/hicolor/scalable/apps/
	$(INSTALL_PROGRAM) mp3metachange.exec /usr/bin/mp3metachange
	gtk-update-icon-cache -f -t /usr/share/icons/hicolor/

uninstall:
	rm -rf /usr/share/mp3metachange
	rm -f /usr/share/applications/mp3metachange.desktop
	rm -f /usr/share/icons/hicolor/scalable/app/mp3metachange.png
	rm -f /usr/bin/mp3metachange
